/*
Các yếu tố xác định lặp đi lặp lại là bài toán lặp
b1: Xác định yếu tố thay đổi (khởi tạo biến bước nhảy)
b2: thiết lập điều kiện lập
b3: cài đặt khối xử lý
b4: thay đổi giá trị của bước nhảy

*/

document.getElementById("btnInTheDiv").onclick = function () {
  //input: number
  var input = document.getElementById("iSo").value * 1;
  //output: string
  var output = "";
  //progress
  var dem = 1;

  while (dem <= input) {
    // còn đúng thì còn làm
    var div = `<div class="alert alert-success mt-2">Hello cybersoft</div>`;
    output = output + div;
    //b4 :
    dem += 1;
  }
  document.getElementById("ketQua1").innerHTML = output;
};

/*--------------Bài 2------------------ */
document.getElementById("btnTinhGiaiThua").onclick = function () {
  //input : number
  var nhapSo = document.getElementById("nhapSo").value * 1;
  //output: string
  var ketQua2 = "";
  //progress
  var giaiThua = 1;
  //   b1: xác định yếu tố thay đổi
  var giaTri = 1;
  //b2 :  xác định điều kiện lặp
  while (giaTri <= nhapSo) {
    // b3 xử lý
    giaiThua *= giaTri;
    // b4
    giaTri += 1;
  }

  document.getElementById("ketQua2").innerHTML = giaiThua;
};
// Bài 3

document.getElementById("btnTinhTong").onclick = function () {
  var nhapSo1 = document.getElementById("nhapSo1").value * 1;
  var dem = 0;
  var ketqua3 = 0;

  while (dem <= nhapSo1) {
    ketqua3 = ketqua3 + dem;
    dem++;
  }
  document.getElementById("ketQua3").innerHTML = ketqua3;
};
// Bài 4

document.getElementById("btnTinhTongSoChan").onclick = function () {
  var nhapSo2 = document.getElementById("nhapSo2").value * 1;
  var ketQua4 = 0;
  var dem = 0;
  while (dem <= nhapSo2) {
    ketQua4 = ketQua4 + dem;
    dem += 2;
  }
  document.getElementById("ketQua4").innerHTML = ketQua4;
};

// Bài 5
document.getElementById("btnTimSoNguyenTo").onclick = function () {
  var nhapSo5 = document.getElementById("nhapSo5").value * 1;
  var output = "";

  var soHang = 1;
  var dem = 0;

  while (soHang <= nhapSo5) {
    if (nhapSo5 % soHang === 0) {
      dem++;
    }
    soHang++;
  }

  if (dem == 2) {
    output = "là số nguyên tố";
  } else {
    output = "không phải là số nguyên tố";
  }

  document.getElementById("ketQua5").innerHTML = nhapSo5 + output;
};
//Bài 6
document.getElementById("btnTimSoNguyenTo6").onclick = function () {
  var nhapSo6 = document.getElementById("nhapSo6").value * 1;
  var output = "";

  // Đặt 1 biến mặc định (biến cờ hiệu)
  var checkSNT = true;
  var soHang = 2;
  while (soHang <= Math.sqrt(nhapSo6)) {
    if (nhapSo6 % soHang == 0) {
      checkSNT = false;
    }
    soHang++;
  }
  if (checkSNT) {
    output = "đây là số nguyên tố";
  } else {
    output = " khong phai";
  }
  document.getElementById("ketQua6").innerHTML = nhapSo6 + output;
};

//bài 7
document.getElementById("btnInNgoiSao7").onclick = function () {
  // var nhapSo7 = document.getElementById("nhapSo7").value * 1;
  // var output = "";
  // var dem = 1;
  // while (dem <= nhapSo7) {
  //   output = output + "*";
  //   dem++;
  // }
  // document.getElementById("ketQua7").innerHTML = output;
  /*AH IU EMMMM */
  var nhapSo7 = document.getElementById("nhapSo7").value * 1;
  var output = "";
  var dem = 1;
  while (dem <= nhapSo7) {
    output = output + dem + " . Anh yêu em" + `<br>`;
    dem++;
  }
  document.getElementById("ketQua7").innerHTML = output;

  // FOR
  // var nhapSo7 = document.getElementById("nhapSo7").value * 1;
  // var output = "";
  // for (var dem = 1; dem <= nhapSo7; dem++) {
  //   output = output + "*";
  // }
  // document.getElementById("ketQua7").innerHTML = output;
};

// Bài 8
document.getElementById("btnInNgoiSao8").onclick = function () {
  // Input: số hàng : number , số cột: number
  var nhapSoHang = document.getElementById("nhapSoHang8").value * 1;
  var nhapSoCot = document.getElementById("nhapSoCot8").value * 1;
  // output string
  var ketQua8 = "";
  //progress

  for (var demHang = 1; demHang <= nhapSoHang; demHang++) {
    // code để sinh ra 1 hàng
    for (var demCot = 1; demCot <= nhapSoCot; demCot++) {
      ketQua8 = ketQua8 + " * ";
    }
    ketQua8 += `<br>`;
  }

  document.getElementById("ketQua8").innerHTML = ketQua8;
};
//bài 9
document.getElementById("btnInSoNguyenTo9").onclick = function () {
  // input:Number
  var nhapSo9 = document.getElementById("nhapSo9").value * 1;
  //output : string
  var output = "";
  var ketQua9 = "";
  for (var dem = 2; dem <= nhapSo9; dem++) {
    var checkSNT = true;
    for (var i = 2; i <= Math.sqrt(dem); i++) {
      if (dem % i === 0) {
        checkSNT = false;
        break;
      }
    }
    if (checkSNT) {
      output = output + dem + " ";
    }
  }
  // for (var dem = 2; dem <= nhapSo9; dem++) {
  //   var soDem = 0;
  //   for (var i = 1; i <= dem; i++) {
  //     if (dem % i == 0) {
  //       soDem++;
  //     }
  //   }
  //   if (soDem == 2) {
  //     output = output + " " + dem;
  //   }
  //   ketQua9 = ketQua9 + output;
  // }

  //   for (var dem = 2; dem <= nhapSo9; dem++) {
  //     var ketqua = songuyento(dem);
  //     output = output + ketqua + " ";
  //   }

  document.getElementById("ketQua9").innerHTML = output;
};

// function songuyento(soNT) {
//   var soDem = 0;
//   var ketqua = "";
//   for (var i = 1; i <= soNT; i++) {
//     if (soNT % i == 0) {
//       soDem++;
//     }
//   }
//   if (soDem == 2) {
//     ketqua = soNT;
//   }
//   return ketqua;
// }
