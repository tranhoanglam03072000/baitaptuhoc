function layThongTinTuForm() {
  var maSV = document.querySelector("#txtMaSV").value;
  var tenSV = document.querySelector("#txtTenSV").value;
  var emailSV = document.querySelector("#txtEmail").value;
  var passSV = document.querySelector("#txtPass").value;
  var diemToan = document.querySelector("#txtDiemToan").value;
  var diemLy = document.querySelector("#txtDiemLy").value;
  var diemHoa = document.querySelector("#txtDiemHoa").value;

  return {
    ma: maSV,
    ten: tenSV,
    email: emailSV,
    pass: passSV,
    diemToan: diemToan,
    diemHoa: diemHoa,
    diemLy: diemLy,
  };
}

function inDssv(arr) {
  var ketQuaInDssv = ``;
  arr.forEach(function (item) {
    var ketQuaInDssv1Hang = `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td>0</td>
    <td>
    <button class="btn btn-success" onclick="xoaSV('${item.ma}')">Xóa</button>
    <button class="btn btn-success" onclick="suaSv('${item.ma}')">Sửa</button>
    </td>
    </tr> `
    ketQuaInDssv += ketQuaInDssv1Hang
  });
  document.querySelector('#tbodySinhVien').innerHTML = ketQuaInDssv
}


function timKiemViTriSv(id,arr){
for(var i =0; i < arr.length; i++){
    if(arr[i].ma === id){
        return i
    }
}
return -1
}


function hienThiThongTinLenForm(arr){
  document.querySelector('#txtMaSV').value = arr.ma
  document.querySelector('#txtTenSV').value = arr.ten
  document.querySelector('#txtEmail').value = arr.email
  document.querySelector('#txtPass').value = arr.pass
  document.querySelector('#txtDiemToan').value = arr.diemToan
  document.querySelector('#txtDiemLy').value = arr.diemLy
  document.querySelector('#txtDiemHoa').value = arr.diemHoa

}