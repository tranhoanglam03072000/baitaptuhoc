var dssv = [];
function saveLocalstorage() {
  var dssvJson = JSON.stringify(dssv);
  //lưu
  localStorage.setItem("DSSV", dssvJson);
}
//lấy
var dssvLocalstorage = localStorage.getItem("DSSV");
//biến về array
if (JSON.parse(dssvLocalstorage)) {
  dssv = JSON.parse(dssvLocalstorage);
  inDssv(dssv);
}
function themSV() {
  var newSV = layThongTinTuForm();
  dssv.push(newSV);
  saveLocalstorage();
  inDssv(dssv);
}

function xoaSV(id) {
  var index = timKiemViTriSv(id, dssv);
  if (index !== -1) {
    dssv.splice(index, 1);
    saveLocalstorage();
  }
  inDssv(dssv);
}

function suaSv(id) {
  var index = timKiemViTriSv(id, dssv);
  if (index !== -1) {
    hienThiThongTinLenForm(dssv[index]);
  }
}
function capNhatSV() {
  var svCapNhat = layThongTinTuForm();
  var index = timKiemViTriSv(svCapNhat.ma, dssv);
  dssv[index] = svCapNhat;
  saveLocalstorage();
  inDssv(dssv);
}
