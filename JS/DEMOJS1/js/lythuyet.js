 // tính nắng cơ bản
      //kiểu dữ liệu cơ bản : number, string, boolean, undefine, null
      var number = 1; // số number: lương, số giờ làm....
      var title = "cybersoft developer"; // chuỗi string : họ tên, địa chỉ, thông tin cá nhân
      var result = true; // giá trị mang tính đúng sai
      var name; // undefine
      var info = null; //null
      //  console.log hiển thị
      console.log(number);
      console.log(title);

      // javascrip sẽ tự hiểu kiểu dữ liệu khi các bạn gán kiểu dữ liệu cho nó
      console.log(typeof title);

      //   TOÁN TỬ
      var soHang1 = 5;
      var soHang2 = 10;
      var tong = soHang1 + soHang2;
      var tich = soHang1 * soHang2;
      var thuong = soHang2 / soHang1;
      var hieu = soHang1 - soHang2;
      var chiadu = soHang2 % soHang1;
      console.log("tong =", tong);
      console.log("tích =", tich);
      console.log("thương =", thuong);
      console.log("hiệu=", hieu);
      console.log("du =", chiadu);
      var bienA = "5";
      var bienB = 10;

      console.log(bienA + bienB);

      // đối với các phép tính số học như cộng trừ nhân chia thì trong lập trình JS xử lý giống hệt của toán
      // JS hỗ trợ toán tử chia lấy dư để lấy kết quả phần dư xử lý
      // Trong js đặc biệt đối với phép cộng thì js xử lý + 2 số ( number ) thì ra giá trị tổng, tuy nhiên cộng 2 chuỗi thì sẽ là nối 2 chuỗi lại với nhau

      var i = 0;
      i = i + 1;
      i++;
      i *= 5;
      console.log("i=", i);

      const HE_SO_LUONG= 10;
    //   var HE_SO_LUONG=10;