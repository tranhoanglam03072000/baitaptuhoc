document.getElementById("btntinhluong").onclick = function () {
  // input: nhapsogiolam:number , nhaptiencong:number
  var nhapSoGioLam = Number(document.getElementById("nhapsogiolam").value);
  var nhapTienCong = Number(document.getElementById("nhaptiencong").value);
  //output:  tienluong:number
  var tienLuong = 0;
  //progress
  if (nhapSoGioLam <= 40) {
    tienLuong = nhapSoGioLam * nhapTienCong;
  } else {
    tienLuong = 40 * nhapTienCong + (nhapSoGioLam - 40) * (nhapTienCong * 1.5);
  }
  document.getElementById("tienluong").innerHTML =
    "Tiền lương của bạn là : " + tienLuong;
};
