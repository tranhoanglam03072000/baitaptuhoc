document.getElementById("btnketqua").onclick = function () {
  //input: diemly,diemtoan,diemhoa : Number
  var nhapDiemLy = Number(document.getElementById("nhapdiemly").value);
  var nhapDiemHoa = Number(document.getElementById("nhapdiemhoa").value);
  var nhapDiemToan = Number(document.getElementById("nhapdiemtoan").value);
  //output: diemtb : number , xeploai: string
  var diemTb = 0;
  var xepLoai = "";
  //progress
  diemTb = (nhapDiemHoa + nhapDiemLy + nhapDiemToan) / 3;
  if (diemTb < 0) {
    diemTb = "Không có";
    xepLoai = " Bớt khùng nha m !!";
  } else if (diemTb > 0 && diemTb <= 5) {
    xepLoai = "Không đạt";
  } else if (diemTb >= 5 && diemTb < 8) {
    xepLoai = "Đạt";
  } else if (diemTb >= 8 && diemTb <= 10) {
    xepLoai = "Giỏi";
  } else {
    diemTb = "Không có";
    xepLoai = "Bớt ảo tưởng nha m!!";
  }
  document.getElementById("ketqua").innerHTML =
    "Điểm TB : " + diemTb + " --" + "Xếp loại : " + xepLoai;
};

/* 
Biểu thức 3 ngôi
if (dk){
  xu_ly1 = gia_tri
} else {
  xu_ly2 = gia_tri
}

dk === true ? xu_ly1; xu_ly2
*/
var a = 13;
var b = -10;
var output = "";
// if (a % 2 == 0) {
//   output = "số chẵn";
// } else {
//   output = "số lẻ";
// };
output = a > 0 && b < 0 ? 7 : 8;
console.log(output);
