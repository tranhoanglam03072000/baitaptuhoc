var dssv = [];
// lấy Localstorage đã lưu lên
var dssvLocalStorage = localStorage.getItem("DSSV");
// Biến localstorage đã lấy thành aray
if (JSON.parse(dssvLocalStorage)) {
  dssv = JSON.parse(dssvLocalStorage);
  inDSSV();
}
function saveLocalStorage() {
  //biến dssv thành JSON
  var dssvJSON = JSON.stringify(dssv);
  //  lưu dssvJSON dưới 1 cái tên
  localStorage.setItem("DSSV", dssvJSON);
}
function themSV() {
  var newSV = layThongTinTuForm();
  dssv.push(newSV);
  saveLocalStorage();
  inDSSV();
}
function xoaSV(id) {
  var index = timViTriSV(id, dssv);
  if (index !== -1) {
    dssv.splice(index, 1);
    saveLocalStorage();
  }
  inDSSV(dssv);
}
function suaSV(id) {
  var index = timViTriSV(id, dssv);
  if (index !== -1) {
    document.querySelector("#txtMaSV").value = dssv[index].ma;
    document.querySelector("#txtTenSV").value = dssv[index].ten;
    document.querySelector("#txtEmail").value = dssv[index].email;
    document.querySelector("#txtPass").value = dssv[index].matKhau;
    document.querySelector("#txtDiemToan").value = dssv[index].diemToan;
    document.querySelector("#txtDiemLy").value = dssv[index].diemLy;
    document.querySelector("#txtDiemHoa").value = dssv[index].diemHoa;
  }
}
function capNhatSV() {
  var svCapNhat = layThongTinTuForm();
  var index = timViTriSV(svCapNhat.ma, dssv);
  if (index !== -1) {
    dssv[index] = svCapNhat;
    saveLocalStorage();
  }
  inDSSV(dssv);
}
