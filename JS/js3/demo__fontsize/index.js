document.getElementById("btnzoominfinger").onclick = function () {
  //input :fontsize
  zoom(5, "midfinger");
  domID("midfinger").style.fontSize = "500px";
  console.log("domID: ", domID);
};
document.getElementById("btnzoomoutfinger").onclick = function () {
  zoom(-5, "midfinger");
};
function domID(id) {
  return document.getElementById(id);
}

function zoom(fontsize, id) {
  var tag = domID(id).style.fontSize;
  var fSize = tag.replace("px", "") * 1;
  fSize += fontsize;
  domID(id).style.fontSize = fSize + "px";
}
