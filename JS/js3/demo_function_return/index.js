document.getElementById("btnTinhDiem").onclick = function () {
  //Tính điểm khối A
  //input: DiemToan, DiemLy , DiemHoa : number
  var nhapDiemToan = document.getElementById("nhapDiemToan").value * 1;
  var nhapDiemHoa = document.getElementById("nhapDiemHoa").value * 1;
  var nhapDiemLy = document.getElementById("nhapDiemLy").value * 1;
  //output: dtbKhoiA: number
  var dtbKhoiA = tinhDiemTB(nhapDiemToan, nhapDiemLy, nhapDiemHoa);
  document.getElementById("dtbKhoiA").innerHTML = dtbKhoiA;

  //Tính điểm Khối C
};
function tinhDiemTB(diem1, diem2, diem3) {
  //input:
  var dtb = 0;
  dtb = (diem1 + diem2 + diem3) / 3;
  //output: diemTrungBinh : number
  return dtb;

  //clg sau lệnh return chương trình sẽ dừng
}
