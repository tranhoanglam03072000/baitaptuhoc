// document.getElementById("pthongtin").innerHTML = "hello word";

function inNoiDung(id, noiDung) {
  //input:tham số
  document.getElementById(id).innerHTML = noiDung;

  //output:
} //=>>> khai báo : chưa gọi hàm
//gọi hàm
inNoiDung("pthongtin", "hellocyber soft");
inNoiDung("sthongtin", "alooooo");

/*Hàm có giá trị trả về 
function return
*/
// var soGiolam = 5;
// var tienLuongTrenGio = 100;
// var tienLuong = soGiolam * tienLuongTrenGio;

function tinhLuong(soGiolam, tienLuongTrenGio) {
  //5 100
  //xử lý
  var tienLuong = soGiolam * tienLuongTrenGio;
  //   console.log("tienLuong: ", tienLuong);

  //output:
  return tienLuong;
}

var tien = tinhLuong(5, 100000);
tien += 100;
console.log("tien: ", tien);

//xử lý thêm
