var arrName = ["nam", "hằng", "minh", "long", "lan"];
console.log("arrName: ", arrName);

// Lấy ra 1 giá trị trong mảng
document.getElementById("hoten").innerHTML = arrName[4];
//Sửa
arrName[1] = "sang";
// Lấy ra số lượng phần tử của mảng

arrName.length;
console.log("arrName.length: ", arrName.length);
//duyệt mảng : In ra màn hình consosole của browser
var ketQua = "";
for (var i = 0; i < arrName.length; i++) {
  ketQua = ketQua + `<p> ${arrName[i]} </p>`;
}
document.getElementById("ketqua").innerHTML = ketQua;
/*



//Hàm thêm 1 hoặc nhiều giá trị vào cuối mảng
arrName.push("Uyên", "Huyền");
//unshift(): thêm 1 hoặc nhiều giá trị vào đầu mảng(Làm thay đổi chỉ số index nên ít xài lắm)
arrName.unshift("Bảo");
console.log("arrName: ", arrName);



*/

//Hàm xóa giá trị ra khỏi mảng
// splice () : Xóa 1 hoặc nhiều giá trị ra khỏi mảng
// arrName.splice(3, 1);
// console.log("arrName: ", arrName);

//shift() : Lấy ra và xóa thằng ml đó khỏi mảng , pop():Lấy ra 1 phần tử ở cuối mảng và xóa nó đi

var hoTen1 = arrName.shift();
console.log("hoTen1: ", hoTen1);

var hoTen2 = arrName.pop();
console.log("hoTen2: ", hoTen2);

//--------------Dom qua tag name ---------------//

var arrTagSection = document.getElementsByTagName("section");
console.log("arrTagSection: ", arrTagSection);
arrTagSection[1].innerHTML = "hello ";
arrTagSection[1].style.color = "blue";

for (var i = 0; i < arrTagSection.length; i++) {
  arrTagSection[i].innerHTML = "anh yêu em";
}

//------------Dom qua class name--------//
var arrTagClass = document.getElementsByClassName("txt");
console.log("arrTagClass: ", arrTagClass);

for (var i = 0; i < arrTagClass.length; i++) {
  arrTagClass[i].innerHTML = "Em yêu";
}

/*===========
.querySelector :  Khi DOM đến dựa vào queryselector thì kết quả trả về là 1 thẻ đầu tiên khớp với selector đó (dù có nhiều thẻ selector giống nhau)
*/

document.querySelector("#btnsubmit").onclick = function () {
  var pText1 = document.querySelector("#p1-text");
  pText1.style.color = "yellow";
};

document.querySelector("#btnDangNhap").onclick = function (event) {
  event.preventDefault();
  var arrTagInput = document.querySelectorAll("#form input");
  console.log("arrTagInput: ", arrTagInput);

  console.log(arrTagInput[0].value);
  console.log(arrTagInput[1].value);
};

// indexof
