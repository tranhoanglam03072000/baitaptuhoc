let hocSinh = [
  { Mahs: 1, ten: "lam" },
  { Mahs: 2, ten: "lam" },
  { Mahs: 3, ten: "hang" },
  { Mahs: 4, ten: "lamds" },
  { Mahs: 5, ten: "lamsnag" },
  { Mahs: 6, ten: "lamne" },
  { Mahs: 7, ten: "lambe" },
  { Mahs: 8, ten: "lam" },
  { Mahs: 9, ten: "lam" },
  { Mahs: 10, ten: "lam" },
];

let hocSinhLam = hocSinh.filter((tenhs) => tenhs.ten == "lam");
console.log("hocSinhLam: ", hocSinhLam);

// tim hs co mahs la 8
let hocsinh3 = hocSinh.find((hs3) => hs3.Mahs == 3);
console.log("hocsinh3: ", hocsinh3);

let viTriHocSinh3 = hocSinh.findIndex((vitri) => vitri.Mahs == 5);
console.log("viTriHocSinh3: ", viTriHocSinh3);

let mang = hocSinh.map((hocsinh, index) => {
  if (hocsinh.Mahs % 2 == 0) {
    return hocSinh;
  }
  return 0;
});

let tinhTongMaHs = hocSinh.reduce((tongMaHs, mahs, index) => {
  return (tongMaHs += mahs.Mahs);
}, 0);

console.log(tinhTongMaHs);

let mangHocSinhChan = hocSinh.reduce((mangHocSinhchana, hocsinh, index) => {
  if (hocsinh.Mahs % 2 == 0) {
    mangHocSinhchana.push(hocsinh);
  }
  return mangHocSinhchana;
}, []);

console.log(mangHocSinhChan);

// dảo
let daonguoc = hocSinh.reverse();
console.log("daonguoc: ", daonguoc);
console.log("hocSinh: ", hocSinh);
